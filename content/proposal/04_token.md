---
title: 4. Governance Token
date: 2022-03-26
url: /proposal/token
---

# Why a token?
With the LFU token we provide the minimum functionality to access to the project governance. 
The main idea is that every action done in the project by the users provides some kind of award in the project. 
But we don't want the token will be used for trading, the main use for the token will be voting in the governance decisions. 

# Tokenomics
- Max Supply: 100.000.000 
- Dev Team: 5%, with 3 years of total vesting, will be unlocked month by month proportionally.
- Seed Fund: 5% of the Total Supply
- First Public ICO: 5% of the total supply
- Liquidity Pool: 5% of the Total Supply
- Community based mint: 80% of the Total Supply
# Minting Tokens
To mint the 80% of the Total Supply it depends on the real users of the project.
Example of Actions that allows to gain LFU Tokens:
- Register as Channel creator and configure the tiers: 10 LFU
- Publish one Content in the channel as Channel Creator: 1 LFU
- Subscriber registers to a Channel, the Channel Creator receives: 0.1 LFU 
- Register as Channel Subscriber at a channel: 1 LFU
- Any Further user action we can define in the future

Note: All those minted token will be limited by day and will have halvings every 3 months to reduce the amount of tokens minted ny day and halving

# Burning tokens
In order to balance the price of the token we need to burn some tokens, when a users perform some actions in the protocol.
Such as: 
- Claiming LFU Tokens
- Voting in the Governance
- Access Advanced Features provided by the project

# Seed Fund

We need to fund the project development with the help of a private Seed ICO.
We propose to sell the 5% of the token with an initial price of 0.05$ for each token
Vesting: 
- Releasing 25% at the first public ICO
- the rest 75%, in 25% fractions every two months

# Public ICO

We need to fund the project development with the help of a private Seed ICO.
We propose to sell the 5% of the token with an initial price of 0.10$ for each token
Vesting:
- Releasing 10% at the first public ICO
- the rest 90%, in 10% fractions every month


## Final Notes
All these percentages and fees we are debating them right now and cloud be modified in the near future. 

Next section: [RoadMap]({{< ref "05_roadmap.md" >}} "RoadMap")
