---
title: 1. Main Goals
date: 2022-03-26
url: /proposal/goals
---


In this section we will describe the main goals for this project:
## Main Goals
- The main goal is to offer to the project and content creators a way to maximize the percentage of revenue from the donations.
- All the decisions will be agreed with the community, first in the project page, after in a [DAO] (https://en.wikipedia.org/wiki/Decentralized_autonomous_organization)
- The fees to pay as creator and as subscriber or supporter must be the lowest possible meanwhile the LibraFund Project is sustainable.
- The platform has to offer a way to define and manage channels and subscriptions.
- The platform has to offer a secure way to share content with the actual subscribers.
- The platform has to be transparent in al the process: payments, decisions, roadmaps...
- The platform will be developed on different blockchains to achieve the previous goals.
- The LibraFund the project must be economically sustainable through its treasury.


Next on [Platform Profiles]({{< ref "02_profiles.md" >}} "Platform Profiles")
