---
title: 7. Future features
date: 2022-03-26
url: /proposal/future
---

# Future features?
We are analyzing some features that will be great to include in the project.
Such as:
- Create NFT owned by the Channel Creator for every content published
- Allow to donate some income percentage to charity causes
- Allow donating money and subscriptions to another Channel Creators
- Allow to associate another address for Channel account advisor
- Allow direct or deferred subscription funds to minimize transactions fees
- Allow subscriber voting to choose the next content to share
- Develop a WordPress/Woocommerce Plugin for managing the membership and the access to the contents

# Feedback 

If you want to contact us for more features you can do it by mail at <pepesan at gmail dot com> or <david at cursosdedesarrollo dot com> or at the [Project Issue Page](https://gitlab.com/pepesan/librefund-whitepaper/-/issues)



