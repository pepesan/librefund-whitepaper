---
title: 3. DAO
date: 2022-03-26
url: /proposal/dao
---

# Why a DAO?

The DAO or [Decentralized autonomous organization](https://en.wikipedia.org/wiki/Decentralized_autonomous_organization) is the best way to share decide the next steps for the project.
It's a democratic way to change the rules about how the project works.

Other crypto projects, platform and protocols has a related DAO's Like [Anchor Protocol](https://www.anchorprotocol.com/#) or [Olympus DAO](https://www.olympusdao.finance/).

Note: In a first milestone the project will be released with a multi-sig account as admin for the project. But in a medium term will be replaced by a dao address to manage all the decisions in a consensus way.

To make this governance process possible it will be necessary to mint a new governance token of the project named LFU.


In the next section we will explain the [Token]({{< ref "04_token.md" >}} "Token")
