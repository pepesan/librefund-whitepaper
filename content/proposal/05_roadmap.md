---
title: 5. Roadmap
date: 2022-03-26
url: /proposal/roadmap
---

# Why a roadmap?
The roadmap show the next steps for the project.
# 1st Month
- Preliminary Landing Page
- Preliminary White Paper
- Presentation on Youtube/Twitch 
- Open discussion about the white paper with the content creator community at the [Project Issue Page](https://gitlab.com/pepesan/librefund-whitepaper/-/issues)
- Blockchain Research for minimum transaction and smart contract fees
# 2nd Month Backend, FrontEnd and BlockChain
- Version 0.0.1: Testing: LFU Token ERC20 Creation
- Version 0.0.2: Testing: Login With metamask
- Version 0.0.2: Testing: Channel Creator Registration
- Version 0.0.3: Testing: Channel Creator Tier Definitions
# 3rd Month Backend, FrontEnd and BlockChain
- Version 0.0.4: Testing: Channel Subscriptions for Subscribers
- Version 0.0.5: Testing: Channel Subscriptions for Channel Creators
- Version 0.0.6: Testing: Channel Creator Private Content Upload
- Version 0.0.7: Testing: Channel Creator Analytics
# 4th Month
- Contracts Certification
- Bug Fixing
# 6th Month
- Version 0.1.1: Production: LFU Token ERC20 Creation
- Version 0.1.2: Production: Login With metamask
- Version 0.1.2: Production: Channel Creator Registration
- Version 0.1.3: Production: Channel Creator Tier Definitions
# 7th Month
- Version 0.1.4: Production: Channel Subscriptions for Subscribers
- Version 0.1.5: Production: Channel Subscriptions for Channel Creators
- Version 0.1.6: Production: Channel Creator Private Content Upload
- Version 0.1.7: Production: Channel Creator Analytics

Next section: [Project Funding]({{< ref "06_funding.md" >}} "Project Funding")
