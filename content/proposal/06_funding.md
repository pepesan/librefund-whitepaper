---
title: 6. Project funding
date: 2022-03-26
url: /proposal/funding
---

# Why a sustainable funding?
The project needs to be financed in a middle and long term. 
The main goal is the treasury will be enough to continue the project development. 
So the first functional project version will be financed by the developers.
But the next financing rounds will use the project treasury.

# The 1% fee

The main idea of this project is to reduce the amount of fees a Creator has to pay for getting money from the subscribers and supporters

# Early stages
If you want to support this project you can contact us in the <pepesan at gmail dot com> or <david at cursosdedesarrollo dot com>,
or in the following protocols:
- ETH
- Fantom
- BSC
- Polygon/Matic

With the address:  0x7E030257c09caa654ad5c9bD09B9B6eb6c034E08 or librefund.dao

# Seed ICO
Search for:
- VC
- Partners

# First Private ICO



Next section: [Future Features]({{< ref "07_future.md" >}} "Future Features")



