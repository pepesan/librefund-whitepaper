---
title: 2. Platform Profiles
date: 2022-03-26
url: /proposal/profiles/
---

In this section we will describe all the profiles for the project:

# Profiles
- Channel Creator: any actor that wants to receive money from another people or organizations and share content with other people. For example: Content Creators, NGO's, Game Developers, Project Promoters, etc...
- Channel Subscriber: a person or organization that wants to subscribe to a Channel. This actor pays a fee for the subscription defined by the Channel Creator.
- Channel Supporter: a person or organization that wants to support to a Channel with money.
- LibraFund Administrator: a person or organization that manage and take decisions about the LibraFund Project.



# Channel Creator
The Channel creator is the base of LibraFund and the reason for this project.
This actor will be able to:
- Create and Manage Channels: Name, Description and Banner image.
- Associate a blockchain address for payment
- Define the LibraFund percentage, with a minimum 1% fee.
- Define de subscription period
- Define the Channel Tiers, their duration, benefits and the related fees
- Define the content available for each Tier
- The content shared will be a URL content hosted outside the platform
- Show the number of subscribers of the channel
- View the channel analytics filtered by subscription period and tier
- See the historical payment transactions
- Generate a QR code to facilitate the donations and subscriptions


### Advantages
The only requirement for the Channel creator will have a Blockchain Wallet at the selected blockchain with enough funds in the for paying the blockchain 
fees during the Channel creation. Our objective is that creations fees will be the lowest possible, and one of the requirements of the chosen blockchain.   
As the chosen blockchain will have very low fees, there will be no entry fee to LibraFund, the Channel Creator only will have to pay the fees to approve 
and store the information in the blockchain.

## Channel Subscriber
The Channel Subscriber is the main support for the Channel Creators. 
This actor will be able to:
- List all the Channels
- View the different Tiers of each channel, their duration, benefits and the related fees
- Subscribe to all the channels wants
- See all the subscribed channels and their related private content
- See the historical payment transactions


### Advantages
- The only requirement for the Channel Subscriber will have a Blockchain Wallet at the selected blockchain with enough funds in the for paying the subscription fee and the blockchain
fees during the Channel Subscription.
- The subscriber will could see all the transactions from the wallet to the creators wallet and the LibraFund Treasury wallet.

## Channel Supporter
The Channel Supporter is a person that wants to support a Channel Creator in a most casual way.
This actor will be able to:
- List all the Channels
- Donate to a specific Channel
- See all the donations to the different channels.
- See the historical payment transactions


### Advantages
- The only requirement for the Channel Supporter will have a Blockchain Wallet at the selected blockchain with enough funds in the for paying the doantion and the blockchain
  fees during the Donation.
- The subscriber will could see all the transactions from the wallet to the creators wallet and the LibraFund Treasury wallet.


# Conclusions
As we can see we solve the main issues of the current content sharing and payment platforms.  
Next section: [Proposal DAO]({{< ref "03_dao.md" >}} "Proposal DAO")
