---
title: Homepage
---

# Hello 👋

**This is the Librefund's Whitepaper!**

If you want to see a brief explanation about LibreFund it's available on [the main page](https://pepesan.gitlab.io/librefund-landingpages/)

Public Team: Soon!

## Repositories
- [Main Website](https://gitlab.com/pepesan/librefund-landingpages)
- [Whitepaper](https://gitlab.com/pepesan/librefund-whitepaper)

→ [Go to Motivation]({{< ref "01_status.md" >}} "Motivation")
