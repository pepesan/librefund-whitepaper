---
title: 1. Status
date: 2022-03-26
url: /motivation/status
---

In the actual world, we found some problems when we want to start a new project, but the main fundamental one is funding it.

There are many options to share our content, but a lot of limitations when we are trying to monetize our content. 

In this Motivation section we will try to study the status of the different platforms, the monetization and the access to the contents.

Next on [Content Creators]({{< ref "02_content_creators.md" >}} "Content Creators")
)
