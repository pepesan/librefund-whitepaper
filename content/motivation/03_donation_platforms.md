---
title: 3. Donation Platforms
date: 2022-03-26
url: /motivation/donation_platforms
---

# Main Donation Platforms

The actual content creator have some options for accepting donations:
- Patreon
- Paypal
- VRA
- Happyfans
- etc...

In this section we will describe the problems using the different platforms for the content creators

## Patreon
Patreon is the main platform for donating to the content creator content sharing, but it has some issues due to the fees to pay or receive the money from the supporters.
When we are trying to monetize our channels in this way we need to pass some barriers:
- donation limits
- high fees for transaction/donation
- No support for sharing private content

Those conditions limit us to monetize our content from the beginning, they act as a simple gateway for donations. 

### Fees

The fees in Patreon are structured in monthly fees, it depends on the fees paid they offer some extra services to the creator.
As of the date of writing this document:
- Lite: 5% fee, with an extra transaction fee
- Pro: 8% fee, with an extra transaction fee
- Premium: 12% fee, with an extra transaction fee

Those conditions are better that the YouTube or Twitch, this is the reason because a lot of creators become Patreon users.

## Paypal
Paypal is the main platform for internet payments, but it has some issues during the donation process. The main problem is the fees the creator have to pay
from the donations:
- 2,9% fee, with a fixed fee
- 1.20%, with a fixed fee, for more than 10$ or 10€

Those conditions limit us to monetize our content from the beginning, they act as a simple gateway for payments and subscriptions, but not allow to share 
content with the subscriptors.


# Conclusions
As we can see clearly almost all platforms fees are high, with a base fee. So the content creators needs alternatives to get more revenues 
for the same work. So we need a integrated solution to solve those issues. → [Go to Proposal Goals]({{< ref "01_goals.md" >}} "Proposal Goals")
