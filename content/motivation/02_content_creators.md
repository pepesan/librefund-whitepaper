---
title: 2. Content Creators
date: 2022-03-26
url: /motivation/content_creators
---

# Main Content Sharing Platforms

The actual content creator have some options for sharing their content
- Youtube
- Twitch
- Theta
- etc...

In this section we will describe the problems in the different platforms for the content creators

## YouTube
YouTube is the main platform for video content sharing, but it has some issues due to the monetization. When we are trying to monetize our channels we need to 
pass some barriers:
- at least 1000 subs
- at least 4000h viewed in the last year
- pass the YouTube filter about our content

Those conditions prevent us to monetize our content from the beginning, and need to pass the YouTube Criteria about what it is an acceptable content for their 
advertisers and audience.

### Revenues 
The revenues come with a price in the case of YouTube because the money sharing is initially 50% for YouTube and 50% for the content creator before taxes.
This distribution is not equitable, nor fair to the content creator.

## Twitch
YouTube is the main platform for video content sharing, but it has some issues due to the monetization. When we are trying to monetize our channels we need to 
pass some barriers:
- at least 25 streamed hours
- at least 12 streamed days
- at least 75 viewers on average
- pass the Twitch filter about our content

Those conditions prevent us to monetize our content from the beginning, but in n easy way compared with YouTube, but we need to pass also the Twitch Criteria about what it is an acceptable content for their
advertisers and audience.

### Revenues
The revenues also come with a price in the case of Twitch because the money sharing is initially 50% for Twitch and 50% for the content creator before taxes.
This distribution is not equitable, nor fair to the content creator.

# Conclusions
As we can see clearly almost all platforms shares are not equitable, nor fair to the content creator. So the content creators needs alternatives to get more revenues 
for the same work, and there are solutions to get this goal as we can see in the next section: [Donation Platforms]({{< ref "03_donation_platforms.md" >}} "Donation Platforms")
